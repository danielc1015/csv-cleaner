/**
 * Given a raw csv line with multiple semicolon in a row,
 * returns the same line in a correct format
 * 
 * @param {string} line 
 * @returns formatedLine
 */
const cleanSemicolon = (line) => {
    console.log('Linea bruta: ', line);
    line = line.split(';');
    console.log('linea separada', line);

    let formatedLine = '';
    let semicolonPlaced = false;

    for (let i = 0; i < line.length; i++) {
        formatedLine += line[i];

        // Verifies that the semicolon has already been placed, but skip at the end
        if (i < line.length - 1) {
            formatedLine += semicolonPlaced ? '' : ';';
        }

        // In case of multiple semicolon in a row, check if the next fragment was a semicolon or a new one
        semicolonPlaced = line[i + 1] == '' ? true : false;
    }
    console.log('Formateado: ', formatedLine);
    return formatedLine;
}

/**
 * Format excel currency format numbers to string without '$' or '.'
 * 
 * @param {string} line 
 * @returns formatedLine
 */
const currencyFormatToString = (line) => {

    // Remove dot
    let lineWithoutDot = '';
    for (let j = 0; j < line.length; j++) {
        if (line.charAt(j) == '.' && isNumber(line.charAt(j+1)) ) {
            continue;
        } else {
            lineWithoutDot += toUTF8(line.charAt(j));
        }
    }

    // Remove $
    lineWithoutDot = lineWithoutDot.split('$');
    let formatedLine = '';

    lineWithoutDot.forEach(element => {
        formatedLine += element;
    });

    return formatedLine;
}

/**
 * 
 * @param {array}} lines 
 * @returns formatedDocumentBody
 */
const toHubExpectedFormat = (lines) => {
    formatedDocument = '';

    lines.forEach(line => {
        newLine = cleanSemicolon(line);
        newLine = currencyFormatToString(newLine);
        formatedDocument += newLine + '\n';
    });

    return formatedDocument;
}

const downloadDocument = (body) => {
    csv = 'data:text/csv;charset=utf-8,' + body;
    let data = encodeURI(csv);
    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', 'formateado.csv');
    link.click();
}

const readFile = async () => {
    // open file picker
    [fileHandle] = await window.showOpenFilePicker();

    console.log(fileHandle.type);
    if (fileHandle.type === 'file') {
        console.log('file');
    } else if (fileHandle.type === 'directory') {
        console.log('file');
    }
}

const isNumber = (numberInStr) => {
    return parseInt(numberInStr) || numberInStr == '0' ? true : false;
}

const toUTF8 = (char) => {
    let newChar;
    switch (char) {
        case 'á':
            return '&aacute;';
        case 'é':
            return '&eacute;';
        case 'í':
            return '&iacute;';
        case 'ó':
            return '&oacute;';
        case 'ú':
            return '&uacute;';
        default:
            return char;
    }
}

let fileHandle;
document.getElementById('fileButton').addEventListener('click', async () => {
    readFile();
});
document.getElementById('inputfile')
    .addEventListener('change', function () {

        var fr = new FileReader();
        fr.onload = function () {
            document.getElementById('output')
                .textContent = fr.result;
            const content = fr.result;
            lines = content.split('\n');
            formatedDocument = toHubExpectedFormat(lines);
            //downloadDocument(formatedDocument);
            console.log(formatedDocument);
        }

        fr.readAsText(this.files[0]);

    })

